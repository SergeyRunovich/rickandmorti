//
//  NetWork.swift
//  TestRikiAndMorti
//
//  Created by Сергей Рунович on 27.02.21.
//

import Foundation

class Network   {
    func fetchData(_ page: Int,complition: @escaping (RikiModel?) -> Void ) {
        if let url = URL(string: "https://rickandmortyapi.com/api/character/?page=\(page)") {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    print(error)
                }
                if let response = response {
                
                }
                if let data = data {
                    let rikiData = try? JSONDecoder().decode(RikiModel.self, from: data)
                    complition(rikiData)
                    
                }
                
            
                
                
                
            }
            
            task.resume()
        }
    }
}
