//
//  TableCell.swift
//  TestRikiAndMorti
//
//  Created by Сергей Рунович on 27.02.21.
//

import UIKit
import Kingfisher

class TableCell: UITableViewCell {
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageLabel: UIImageView!
    @IBOutlet weak var speciesLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    
    static var identifier = "tableCell"
    static var nib = "TableCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func configur(result: Result?) {
        guard let result = result else { return }
        nameLabel.text = result.name
        if let id = result.id {
            idLabel.text = "\(id)"
        }
        if let image = URL(string: result.image ?? "") {
            imageLabel.kf.setImage(with: image)
            
        }
        
        speciesLabel.text = result.species
        statusLabel.text = result.status
        genderLabel.text = result.gender
        
        
        
    }
   
}
