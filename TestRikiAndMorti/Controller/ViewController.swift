//
//  ViewController.swift
//  TestRikiAndMorti
//
//  Created by Сергей Рунович on 27.02.21.
//

import UIKit

class ViewController: UIViewController {
    
    var rikiData: RikiModel?
    let network = Network()
    var rikiDataArray: [Result] = []
    var currentPage = 1
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            let nib = UINib(nibName: TableCell.nib , bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: TableCell.identifier)
            tableView.rowHeight = 150
            
        }
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchData()
    }
    
    
    func fetchData() {
        network.fetchData(currentPage) { (data) in
            self.rikiData = data
            self.rikiDataArray += data?.results ?? []
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}

extension ViewController:  UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "secondVC") as! SecondView
        vc.person = rikiDataArray[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rikiDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCell.identifier, for: indexPath) as! TableCell
        cell.configur(result: rikiDataArray[indexPath.row])
        
        if indexPath.row == rikiDataArray.count - 1 && currentPage < rikiData?.info?.pages ?? 1 {
            currentPage += 1
            fetchData()
        }
        
        
        return cell
        
        
    }
    
    
}
