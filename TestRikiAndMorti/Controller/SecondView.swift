//
//  SecondView.swift
//  TestRikiAndMorti
//
//  Created by Сергей Рунович on 28.02.21.
//

import UIKit
import Kingfisher

class SecondView: UIViewController {
    var person: Result?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imagePersLabel: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
personSetting()
       
    }
    
    func personSetting() {
        nameLabel.text = person?.name
        statusLabel.text = person?.status
        if let image = URL(string: person?.image ?? "") {
            imagePersLabel.kf.setImage(with: image)
            
        }
    
}
    
    
}
